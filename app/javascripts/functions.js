/**
 * @Author: Jose Manuel Periban Palacios <P5YCH0D3>
 * @Date:   2017-02-22T16:56:14-08:00
 * @Email:  ANON.P5YCH0D3@gmail.com
* @Last modified by:   P5YCH0D3
* @Last modified time: 2017-02-25T11:13:44-08:00
 */



var inUN = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia (Plurinational State of)", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cabo Verde", "Cambodia", "Cameroon", "Canada", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo", "Costa Rica", "Côte D'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Democratic People's Republic of Korea", "Democratic Republic of the Congo", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Fiji", "Finland", "France", "Gabon", "Gambia (Islamic Republic of the)", "Georgia", "Germany", "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kuwait", "Kyrgyzstan", "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia (Federated States of)", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Nambia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Republic of Korea", "Republic of Moldova", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Sudan", "Spain", "SriLanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Tajikstan", "Thailand", "The former Yugoslav Republic of Macedonia", "Timor-Leste", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom of Great Britain and Nothern Ireland", "United Republic of Tanzia", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela,Bolivarian Republic of", "VietNam", "Yemen", "Zambia", "Zimbabwe"];
var d = new Date();
var hour = d.getHours();
var min = d.getMinutes();
var secondsPassed = 1;
var countDown;
var minT, secT;

window.onload = function() {
    var changeHighlighting = setInterval(function() {
        if (hour == 7 && min >= 45) {
            document.getElementById('registration').style.color = "rgb(167, 56, 56)";
        } else if (hour == 8 && min == 30) {
            document.getElementById('openCerem').style.color = "rgb(167, 56, 56)";
        } else if (hour == 9) {
            document.getElementById('committeeS1').style.color = "rgb(167, 56, 56)";
        } else if (hour == 12) {
            document.getElementById('lunchA').style.color = "rgb(167, 56, 56)";
        } else if (hour == 12 && min == 15) {
            document.getElementById('lunchB').style.color = "rgb(167, 56, 56)";
        } else if (hour == 13) {
            document.getElementById('committeeS2').style.color = "rgb(167, 56, 56)";
        } else if (hour == 16 && min == 15) {
            document.getElementById('closingCere').style.color = "rgb(167, 56, 56)";
        } else {
            document.getElementById('schedule1').style.color = "rgb(167, 56, 56)";
        }
    }, 1000);

    for (var c in inUN) {
        var a = document.createElement('button');
        a.id = inUN[c];
        a.innerHTML = inUN[c];
        document.getElementById('dropDownCountries').appendChild(a);
        document.getElementById(inUN[c]).onclick = function() {
            var trs = document.createElement('TR');
            var ths = document.createElement('TH');
            var choosenCountry = document.createTextNode(this.id);
            ths.appendChild(choosenCountry);
            ths.addEventListener("click", function() {
                ths.style.textDecoration = "line-through";
            })
            ths.addEventListener("dblclick", function() {
                ths.remove();
            })
            trs.appendChild(ths);
            document.getElementById('speakerTable').appendChild(trs);
            document.getElementById('dropDownCountries').classList.toggle('show');
        }
    }
}

function showDD() {
    document.getElementById("dropDownCountries").classList.toggle("show");
}

function filterFunc() {
    var input, filter, ul, li, a, i;
    input = document.getElementById('dropID');
    filter = input.value.toUpperCase();
    div = document.getElementById("dropDownCountries");
    buttonEle = div.getElementsByTagName("button");
    for (i = 0; i < buttonEle.length; i++) {
        if (buttonEle[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            buttonEle[i].style.display = "";
        } else {
            buttonEle[i].style.display = "none";
        }
    }
}

function timer(passedID) {
    console.log(passedID);
    if (passedID == "startTs") {
        document.getElementById('resetTs').removeAttribute("hidden");
        document.getElementById(passedID).setAttribute('hidden', '');
        minT = document.getElementById('minTS').value;
        secT = document.getElementById('secTS').value;
        countDown = setInterval(function() {
            if (secT == 0) {
                if (minT != 0) {
                    minT = minT - 1;
                    document.getElementById('minTS').value = minT;
                    secT = 59;
                    document.getElementById('secTS').value = secT;
                } else {
                    clearInterval(countDown);
                    if (secondsPassed) {
                        if (parseInt(secT) + secondsPassed >= 60) {
                            minT = parseInt(minT) + parseInt((secondsPassed + parseInt(secT)) / parseInt(60));
                            secT = parseInt(secT) + secondsPassed;
                            secT = parseInt(secT) - (60 * parseInt((parseInt(secT)) / parseInt(60)));
                        } else if (secT + secondsPassed <= 59) {
                            secT = parseInt(secT) + parseInt(secondsPassed);
                            secT = parseInt(secT) - 1;
                            console.log(secT);
                        }
                    }
                    document.getElementById('minTS').value = minT;
                    document.getElementById('secTS').value = secT;
                    document.getElementById('startTs').removeAttribute('hidden');
                    document.getElementById('resetTs').setAttribute('hidden', '');
                    secondsPassed = 1;
                }
            } else {
                secT = secT - 1;
                secondsPassed = secondsPassed + 1;
                document.getElementById('secTS').value = secT;
                console.log("Speaker: " + secT);
            }
        }, 1000);
    } else if (passedID == "startTc") {
        document.getElementById('resetTc').removeAttribute("hidden");
        document.getElementById(passedID).setAttribute('hidden', '');
        minT = document.getElementById('minTC').value;
        secT = document.getElementById('secTC').value;
        countDown = setInterval(function() {
            if (secT == 0) {
                if (minT != 0) {
                    minT = minT - 1;
                    document.getElementById('minTC').value = minT;
                    secT = 59;
                    document.getElementById('secTC').value = secT;
                } else {
                    clearInterval(countDown);
                    if (secondsPassed) {
                        if (parseInt(secT) + secondsPassed >= 60) {
                            minT = parseInt(minT) + parseInt((secondsPassed + parseInt(secT)) / parseInt(60));
                            secT = parseInt(secT) + secondsPassed;
                            secT = parseInt(secT) - (60 * parseInt((parseInt(secT)) / parseInt(60)));
                        } else if (secT + secondsPassed <= 59) {
                            secT = parseInt(secT) + parseInt(secondsPassed);
                            secT = parseInt(secT) - 1;
                            console.log(secT);
                        }
                    }
                    secondsPassed = 1;
                    document.getElementById('minTC').value = minT;
                    document.getElementById('secTC').value = secT;
                    document.getElementById('startTc').removeAttribute('hidden');
                    document.getElementById('resetTc').setAttribute('hidden', '');
                }
            } else {
                secT = secT - 1;
                secondsPassed = secondsPassed + 1;
                document.getElementById('secTC').value = secT;
                console.log("Comment: " + secT);
            }
        }, 1000);
    } else if (passedID == "stopTs") {
        clearInterval(countDown);
        document.getElementById('startTs').removeAttribute("hidden");
        document.getElementById('resetTs').setAttribute('hidden', '');
        minT = document.getElementById('minTS').value;
        secT = document.getElementById('secTS').value;
    } else if (passedID == "stopTc") {
        clearInterval(countDown);
        document.getElementById('startTc').removeAttribute("hidden");
        document.getElementById('resetTc').setAttribute('hidden', '');
        minT = document.getElementById('minTC').value;
        secT = document.getElementById('secTC').value;
    } else if (passedID == "resetTs") {
        if (secondsPassed) {
            if (parseInt(secT) + secondsPassed >= 60) {
                minT = parseInt(minT) + parseInt((secondsPassed + parseInt(secT)) / parseInt(60));
                secT = parseInt(secT) + secondsPassed;
                secT = parseInt(secT) - (60 * parseInt((parseInt(secT)) / parseInt(60)));
            } else if (secT + secondsPassed <= 59) {
                secT = parseInt(secT) + parseInt(secondsPassed);
                secT = parseInt(secT) - 1;
                console.log(secT);
            }
        }
        clearInterval(countDown);
        document.getElementById('startTs').removeAttribute("hidden");
        document.getElementById('resetTs').setAttribute('hidden', '');
        document.getElementById('minTS').value = minT;
        document.getElementById('secTS').value = secT;
        secondsPassed = 1;
    } else if (passedID == "resetTc") {
        if (secondsPassed) {
            if (parseInt(secT) + secondsPassed >= 60) {
                minT = parseInt(minT) + parseInt((secondsPassed + parseInt(secT)) / parseInt(60));
                secT = parseInt(secT) + secondsPassed;
                secT = parseInt(secT) - (60 * parseInt((parseInt(secT)) / parseInt(60)));
            } else if (secT + secondsPassed <= 59) {
                secT = parseInt(secT) + parseInt(secondsPassed);
                secT = parseInt(secT) - 1;
                console.log(secT);
            }
        }
        clearInterval(countDown);
        document.getElementById('startTc').removeAttribute("hidden");
        document.getElementById('resetTc').setAttribute("hidden");
        document.getElementById('minTC').value = minT;
        document.getElementById('secTC').value = secT;
        secondsPassed = 1;
    }
}

function createNames() {
    document.getElementById('nameOfComL').setAttribute('hidden', '');
    document.getElementById('nameOfComIn').setAttribute('hidden', '');
    document.getElementById('nameOfChairL').setAttribute('hidden', '');
    document.getElementById('nameOfChairIn').setAttribute('hidden', '');
    document.getElementById('nameOfViceChairL').setAttribute('hidden', '');
    document.getElementById('nameOfViceChairIn').setAttribute('hidden', '');
    document.getElementById('nameOfModL').setAttribute('hidden', '');
    document.getElementById('nameOfModIn').setAttribute('hidden', '');
    document.getElementById('namesBtn').setAttribute('hidden', '');
    var comName = document.createElement('div');
    comName.id = "nameofCom";
    comName.innerHTML = document.getElementById('nameOfComIn').value;
    document.getElementById('names').appendChild(comName);
    var chairName = document.createElement('div');
    chairName.id = "nameofChair";
    chairName.innerHTML = document.getElementById('nameOfChairIn').value;
    document.getElementById('names').appendChild(chairName);
    var viceChairName = document.createElement('div');
    viceChairName.id = "nameofViceChair";
    viceChairName.innerHTML = document.getElementById('nameOfViceChairIn').value;
    document.getElementById('names').appendChild(viceChairName);
    var modName = document.createElement('div');
    modName.id = "nameofMod";
    modName.innerHTML = document.getElementById('nameOfModIn').value;
    document.getElementById('names').appendChild(modName);
}
