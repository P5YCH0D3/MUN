/**
 * @Author: Jose Manuel Periban Palacios <P5YCH0D3>
 * @Date:   2017-02-15T13:54:48-08:00
 * @Email:  ANON.P5YCH0D3@gmail.com
* @Last modified by:   P5YCH0D3
* @Last modified time: 2017-02-23T19:21:09-08:00
 */



var electron, path, json, menuT, Menu;
electron = require('electron');
path = require('path');
json = require('../../package.json');
menuT = require('./menu');
Menu = electron.Menu;

electron.app.on('ready', function() {
    var window;

    window = new electron.BrowserWindow({
        title: json.name,
        width: 1366,
        height: 768
    });

    window.loadURL('file://' + path.join(__dirname, '..', '..') + '/index.html');

    var template = menuT.getTemplate();

    if (process.platform === 'darwin') {
        template.unshift({
            label: electron.app.getName(),
            submenu: [{
                    role: 'about'
                },
                {
                    type: 'separator'
                },
                {
                    role: 'services',
                    submenu: []
                },
                {
                    type: 'separator'
                },
                {
                    role: 'hide'
                },
                {
                    role: 'hideothers'
                },
                {
                    role: 'unhide'
                },
                {
                    type: 'separator'
                },
                {
                    role: 'quit'
                }
            ]
        })
        // Edit menu.
        template[1].submenu.push({
            type: 'separator'
        }, {
            label: 'Speech',
            submenu: [{
                    role: 'startspeaking'
                },
                {
                    role: 'stopspeaking'
                }
            ]
        })
        // Window menu.
        template[3].submenu = [{
                label: 'Close',
                accelerator: 'CmdOrCtrl+W',
                role: 'close'
            },
            {
                label: 'Minimize',
                accelerator: 'CmdOrCtrl+M',
                role: 'minimize'
            },
            {
                label: 'Zoom',
                role: 'zoom'
            },
            {
                type: 'separator'
            },
            {
                label: 'Bring All to Front',
                role: 'front'
            }
        ]
    }
    var menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
    window.webContents.on('did-finish-load', function() {
        window.webContents.send('loaded', {
            appName: json.name,
            electronVersion: process.versions.electron,
            nodeVersion: process.versions.node,
            chromiumVersion: process.versions.chrome
        });
    });

    window.on('closed', function() {
        window = null;
    });

});
