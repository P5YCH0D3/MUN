<!--
@Author: Jose Manuel Periban Palacios <P5YCH0D3>
@Date:   2017-02-20T14:52:49-08:00
@Email:  ANON.P5YCH0D3@gmail.com
@Last modified by:   P5YCH0D3
@Last modified time: 2017-02-23T19:49:44-08:00
-->



# MUN

> Toolset for MUN conferences

## Usage:
> Title

Program will automatically arrange them
###### Committee:
```
Name of Committee
```
###### Chair:
```
Name of Chair
```
###### Vice-Chair:
```
Name of Vice Chair
```
###### Moderator:
```
Name of Moderator
```

> Speaker's List
###### Input:
```
Click of Input box to open country list
Clicking on country will add country to speakers list
```
###### Country Name:
```
1. Press one the name once to strike it out
2. Double click to remove from speakers list completely
```
> Timer

```
 ...pls
```

> Schedule
```
Schedule will change highlight events as they pass automatically
```

## Releases:
[Releases in Gitlab](https://gitlab.com/P5YCH0D3/MUN/tags)
## TODO:
[TODO in GitLab](https://gitlab.com/P5YCH0D3/MUN/boards)

## Issues:
[Issues in GitLab](https://gitlab.com/P5YCH0D3/MUN/issues)
## Dev

```
$ npm install
```

### Run

```
$ bozon start
```

### Package

```
$ bozon package
```

Builds the app for OS X, Linux, and Windows, using [electron-builder](https://github.com/electron-userland/electron-builder).


## License

The MIT License (MIT) © Jose Manuel Periban Palacios 2017
