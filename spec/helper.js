var path = require('path');

module.exports = {
  appPath: function() {
    switch (process.platform) {
      case 'darwin':
        return path.join(__dirname, '..', '.tmp', 'MUN-darwin-x64', 'MUN.app', 'Contents', 'MacOS', 'MUN');
      case 'linux':
        return path.join(__dirname, '..', '.tmp', 'MUN-linux-x64', 'MUN');
      default:
        throw 'Unsupported platform';
    }
  }
};
